//
//  EmployeeTableViewController.h
//  jsonTest
//
//  Created by ALi on 07/02/15.
//  Copyright (c) 2015 ALi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmployeeTableViewController : UITableViewController

@property (nonatomic, strong) NSArray *names;

@end
