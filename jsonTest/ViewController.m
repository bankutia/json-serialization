//
//  ViewController.m
//  jsonTest
//
//  Created by ALi on 06/02/15.
//  Copyright (c) 2015 ALi. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSURL *url = [NSURL URLWithString:@"http://tiny.cc/jsonNames"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
    
    NSLog(@"%@", jsonString);
    
    id reader = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    NSLog(@"%@", reader);
    NSLog(@"%@", reader[@"employees"]);
    NSLog(@"%@", reader[@"employees"][0]);
    NSLog(@"%@", reader[@"employees"][0][@"firstName"]);
}


@end
