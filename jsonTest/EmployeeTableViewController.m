//
//  EmployeeTableViewController.m
//  jsonTest
//
//  Created by ALi on 07/02/15.
//  Copyright (c) 2015 ALi. All rights reserved.
//

#import "EmployeeTableViewController.h"

@implementation EmployeeTableViewController {
@private
    __strong UILabel *_backgroundDisplay;
}

- (IBAction)refreshTableView:(id)sender {
    
    self.names = nil;
    [self.tableView reloadData];
    
    [self gatheringDataFromURL];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self gatheringDataFromURL];
}

- (void)gatheringDataFromURL {
    
    // loading data in background
    dispatch_queue_t queueDEF = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queueDEF, ^{
        
        // ---
        // NS-COMMUNICATION
        // :
        // get information from a specified url with NSURL, NSURLRequest and NSURLConnection
        NSURL *url = [NSURL URLWithString:@"http://tiny.cc/jsonNames"];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSError *error = nil;
        NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error];
        if (error != nil) {
            _backgroundDisplay.text = [NSString stringWithFormat:@"Loading data failed!\r\n%@(%ld)", error.localizedDescription, (long)error.code];
            return;
        }
        
        // ---
        // JSON SERIALIZATION
        // :
        // make a serializer and deserialize message
        id jsonData = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
        NSArray *nameList = jsonData[@"employees"];
        NSMutableArray *nameStrings = [NSMutableArray new];
        for (id name in nameList)
            [nameStrings addObject:[NSString stringWithFormat:@"%@ %@", name[@"firstName"], name[@"lastName"]]];
        
        // redraw table with names on UI thread when data-loading has finished
        dispatch_async(dispatch_get_main_queue(), ^{
            self.names = nameStrings;
            [self.tableView reloadData];
        });
    });
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    if (self.names==nil) {
        
        // show note for user while loading data
        _backgroundDisplay = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        _backgroundDisplay.text = @"Loading data in progress...";
        _backgroundDisplay.textColor = [UIColor grayColor];
        _backgroundDisplay.numberOfLines = 0;
        _backgroundDisplay.textAlignment = NSTextAlignmentCenter;
        _backgroundDisplay.font = [UIFont fontWithName:@"HelveticaNeue-Italic" size:[UIFont systemFontSize]*1.5f];
        [_backgroundDisplay sizeToFit];
        
        // ---
        // UITABLEVIEW BACKGROUND
        // :
        // make and connect any view for background behind the UITableView
        self.tableView.backgroundView = _backgroundDisplay;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        return 0;
    }
    
    self.tableView.backgroundView = nil;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.names.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    return @"Employees";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    // Configure the cell...
    cell.textLabel.text = self.names[indexPath.row];
    
    return cell;
}

@end
